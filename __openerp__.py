# description about module 
{
	"name": "Debrand",
	"author": "ehAPI Technologies",
	"version": "1.0.0",
	'author': 'ehAPI Team',
	'description': """This Module is hides Preferences, Odoo support, help options from dropdown, Hides powered by odoo from bottom left and hides links from login page.
	""",

    "category": "Tools",
    "depends": ['base','web'],
    "demo_xml": [],
    "data": [
    'debrand_view.xml',
	],
	"qweb": [
        'static/src/xml/base.xml',
    ],
	'auto_install': False,
	'application': True,
	"installable": True,
}
